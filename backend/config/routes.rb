Rails.application.routes.draw do
  resources :sales
  resources :sales_figure_data
  resources :sales_figures
  resources :salesfigures
  resources :books
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
