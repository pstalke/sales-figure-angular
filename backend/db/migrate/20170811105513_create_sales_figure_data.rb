class CreateSalesFigureData < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_figure_data do |t|
      t.string :name
      t.integer :downloads
      t.decimal :rating
      t.decimal :payout
      t.integer :month

      t.timestamps
    end
  end
end
