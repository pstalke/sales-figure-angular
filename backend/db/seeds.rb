# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

 
Sale.create!([
  { name: 'Ultra Donkey Run', downloads: 2560622, rating: 4.51, payout: 5042.71, month: 0 },
  { name: 'Ultra Donkey Run', downloads: 199402, rating: 3.12, payout: 350.22, month: 1 },
  { name: 'Ultra Donkey Run', downloads: 3069257, rating: 4.99, payout: 12720.55, month: 2 },
  { name: 'Ultra Donkey Run', downloads: 22000, rating: 2.33, payout: 50.12, month: 3 },
  { name: 'Ultra Donkey Run', downloads: 9000830, rating: 4.62, payout: 48994.94, month: 4 }, 
  { name: 'Spider Pig Ultimate', downloads: 5690593, rating: 2.35, payout: 555089.24, month: 0 }, 
  { name: 'Spider Pig Ultimate', downloads: 1035317, rating: 3.84, payout: 53741.64, month: 1 }, 
  { name: 'Spider Pig Ultimate', downloads: 9243648, rating: 2.89, payout: 437562.30, month: 2 }, 
  { name: 'Spider Pig Ultimate', downloads: 4606325, rating: 3.88, payout: 57581.64, month: 3 }, 
  { name: 'Spider Pig Ultimate', downloads: 206078, rating: 1.20, payout: 451883.72, month: 4 }, 
  { name: 'BoomBot', downloads: 356013, rating: 4.74, payout: 67995.06, month: 0 }, 
  { name: 'BoomBot', downloads: 9495399, rating: 5.00, payout: 495823.40, month: 1 }, 
  { name: 'BoomBot', downloads: 2467930, rating: 3.65, payout: 222613.78, month: 2 }, 
  { name: 'BoomBot', downloads: 5292335, rating: 2.13, payout: 64079.45, month: 3 }, 
  { name: 'BoomBot', downloads: 9249441, rating: 1.31, payout: 55590.80, month: 4 }, 
  { name: 'Wiggly Wally', downloads: 1989445, rating: 4.20, payout: 46685.23, month: 0 }, 
  { name: 'Wiggly Wally', downloads: 4438658, rating: 2.26, payout: 168198.43, month: 1 }, 
  { name: 'Wiggly Wally', downloads: 2385045, rating: 3.93, payout: 338955.76, month: 2 }, 
  { name: 'Wiggly Wally', downloads: 6179542, rating: 2.57, payout: 218491.79, month: 3 }, 
  { name: 'Wiggly Wally', downloads: 4074360, rating: 1.36, payout: 70893.95, month: 4 }
])
