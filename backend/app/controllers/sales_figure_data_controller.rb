class SalesFigureDataController < ApplicationController
  before_action :set_sales_figure_datum, only: [:show, :update, :destroy]

  # GET /sales_figure_data
  def index
    @sales_figure_data = SalesFigureData.all

    render json: @sales_figure_data
  end

  # GET /sales_figure_data/1
  def show
    render json: @sales_figure_datum
  end

  # POST /sales_figure_data
  def create
    @sales_figure_datum = SalesFigureData.new(sales_figure_datum_params)

    if @sales_figure_datum.save
      render json: @sales_figure_datum, status: :created, location: @sales_figure_datum
    else
      render json: @sales_figure_datum.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /sales_figure_data/1
  def update
    if @sales_figure_datum.update(sales_figure_datum_params)
      render json: @sales_figure_datum
    else
      render json: @sales_figure_datum.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sales_figure_data/1
  def destroy
    @sales_figure_datum.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_figure_datum
      @sales_figure_datum = SalesFigureData.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sales_figure_datum_params
      params.require(:sales_figure_datum).permit(:name, :downloads, :rating, :payout, :month)
    end
end
