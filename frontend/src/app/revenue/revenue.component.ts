import { HttpRequestsService } from '../services/http-requests.service';
import { Component, OnInit } from '@angular/core';
import { DevService } from '../services/dev.service';

// import { IDropdownItem } from 'ng2-dropdown-multiselect';

declare var require: any;

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})



export class RevenueComponent implements OnInit {
  salesData: any;
  dateStartOpen: boolean;
  dateEndOpen: boolean;
  appsList = [];
  tgList = [];
  appsSelected = [];
  tgSelected = [];
  appsDropdownSettings = {};
  tgDropdownSettings = {};

  public analyticsChartData: any[];
  public revenueChartData: any[];
    
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    maintainAspectRatio: false,
    responsive: true
  };
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public dateStart: any = new Date();
  public dateEnd: any = new Date();
  public date1: any = new Date();
  public date2: any = new Date();
  public minDate: Date = void 0;
  public dateDisabled: { date: Date, mode: string }[];
  public formats: string[] = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY',
    'shortDate'];
  public format: string = this.formats[0];
  public dateOptions: any = {
    formatYear: 'YY',
    startingDay: 1
  };
  private opened: boolean = false;
  today: Date = new Date();
  dateOnStart: any;
  dateOnEnd: any;

  public getDateEnd(): number {
    this.dateEnd = this.date2;
    return this.dateEnd;
  }

  public getDateStart(): number {
    this.dateStart = this.date1;
    return this.dateStart;
  }

  public open(): void {
    this.opened = !this.opened;
  }

  constructor(private http: HttpRequestsService) {
    // this.appsOptions = [
    //         { id: 1, name: 'Option 1' },
    //         { id: 2, name: 'Option 2' },
    //     ];
    // this.dropdownModel = [
    //     {
    //         id: 1,
    //         label: 'Today',
    //         selected: false, // optional
    //         color: '#336699' // optional
    //     }
    // ];
    this.dateStartOpen = false;
    this.dateEndOpen = false;
  }

  async ngOnInit() {
    this.appsList = [
                      {"id":1,"itemName":"All"},
                      {"id":2,"itemName":"None"},
                      {"id":3,"itemName":"Build a Bridge!"},
                      {"id":4,"itemName":"City Driver: Roof Parking Challenge"},
                      {"id":5,"itemName":"Coast Guard: Beach Rescue Team"},
                      {"id":6,"itemName":"Dancing Ball"},
                      {"id":7,"itemName":"Monaco Parking"},
                      {"id":8,"itemName":"Multi 7 Car Parking Garage Park Training Lot"},
                      {"id":9,"itemName":"Pharaoh's Slots"},
                      {"id":10,"itemName":"Spike Tower"},
                      {"id":11,"itemName":"Through The Fog"},
                      {"id":12,"itemName":"Tiny Gladiators"}
                    ];
    this.tgList = [
      {"id":1,"itemName":"All"},
      {"id":2,"itemName":"None"},
      {"id":12,"itemName":"Tiny Gladiators"}
    ];
    this.appsDropdownSettings = { 
                              singleSelection: false, 
                              text:"Testcase Folder",
                              enableSearchFilter: false,
                              enableCheckAll: false,
                              classes:"myclass custom-class"
                            }; 
    this.tgDropdownSettings = { 
      singleSelection: false, 
      text:"Tiny Gladiators Alone",
      enableSearchFilter: false,
      enableCheckAll: false,
      classes:"myclass custom-class"
    }; 
    // this.salesData = await this.http.getData();
    this.salesData = require('../../assets/sales_figures.json');
    DevService.log(this.salesData);
    this.analyticsChartData = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ]
    this.revenueChartData = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ]
  }

}
