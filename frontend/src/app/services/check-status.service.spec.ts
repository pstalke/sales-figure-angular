import { TestBed, inject } from '@angular/core/testing';

import { CheckStatusService } from './check-status.service';

describe('CheckStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckStatusService]
    });
  });

  it('should be created', inject([CheckStatusService], (service: CheckStatusService) => {
    expect(service).toBeTruthy();
  }));
});
