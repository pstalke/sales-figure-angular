import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class DevService {
  static log(message: any, trace?: boolean) {
    if (!environment.production) {
      console.log(message);

      if (trace) {
        console.log('[DEBUG PLACED IN:]');
        console.trace();
      }
    }
  }

  static warning(warning: any, trace?: boolean) {
    if (!environment.production) {
      console.warn(`${warning}`);

      if (trace) {
        console.log('[WARNING PLACED IN:]');
        console.trace();
      }
    }
  }

  static error(error: any) {
    if (!environment.production) {
      throw new Error(error);
    }
  }

  constructor() { }

}
