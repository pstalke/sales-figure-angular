import { tryCatch } from 'rxjs/util/tryCatch';
import { _catch } from 'rxjs/operator/catch';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

@Injectable()
export class HttpRequestsService {

  constructor(private http: Http) { }

  // Function for GET requestsą

  getResponse(link: string): Observable<any[]> {
    return this.http.get(link)
      .map(this.extractData)
      .catch(this.handleError);
  }

  async getData(): Promise<number> {
    const response = await this.http.get('http://localhost:3000/sales_figures.json').toPromise();
    return response.json();
  }

  // Function to extract data from GET response

  private extractData(res: Response) {
    let body = res.json();
    return body || {}
  }

  // Function to show errors from GET response

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}
