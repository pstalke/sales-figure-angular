import { Injectable } from '@angular/core';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';

@Injectable()
export class CheckStatusService {

  logged: boolean;
  userType: string;

  constructor(private router: Router) {
    this.logged = false;
    this.userType = 'loggedOut';
  }

  checkStatus = function () {
    // if (!AppComponent.incognito) {
      if (localStorage.getItem('logged') === 'true') {
        this.logged = true;
        // if (localStorage.getItem('userType') === 'f') {
        //   this.userType = 'fan';
        // } else if (localStorage.getItem('userType') === 'app') {
        //   this.userType = 'artist';
        // } else if (localStorage.getItem('userType') === 'v') {
        //   this.userType = 'venue';
        // } else {
        //   this.userType = 'loggedout';
        // }
        AppComponent.userLoggedIn = true;
        // console.log(location.pathname);
        // if (location.pathname === '/') {
          this.router.navigate([`./revenue`], { preserveQueryParams: true });
        // };
      } else {
        this.logged = false;
        this.userType = 'loggedOut';
        AppComponent.userLoggedIn = false;
        // if (location.pathname !== '/signup' && location.pathname !== '/privacy' && location.pathname !== '/terms' && location.pathname !== '/forgotpassword' && location.pathname !== '/forgot_password_sent') {
          this.router.navigate([`./signin`], { preserveQueryParams: true });
        // }
      };
    // } else {
    //   this.router.navigate([`./nolcl`], { preserveQueryParams: true });
    // }
  };
  getStatus = function () {
    return this.logged;
  };
  getUserType = function () {
    return this.userType;
  }
}
