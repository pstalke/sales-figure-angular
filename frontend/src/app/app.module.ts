import { HttpModule } from '@angular/http';
import { DevService } from './services/dev.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RevenueComponent } from './revenue/revenue.component';
import { FormsModule } from '@angular/forms';

import { HttpRequestsService } from './services/http-requests.service';
import { CheckStatusService } from './services/check-status.service';
import { RankingsComponent } from './rankings/rankings.component';
import { SettingsComponent } from './settings/settings.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import 'rxjs/Rx';

const appRoutes: Routes = [
  {
    path: '',
    component: SignInComponent
  },
  {
    path: 'signin',
    component: SignInComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'revenue',
    component: RevenueComponent
  },
  {
    path: 'rankings',
    component: RankingsComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: '**',
    component: DashboardComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    SignInComponent,
    DashboardComponent,
    RevenueComponent,
    RankingsComponent,
    SettingsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: false }),
    BrowserModule,
    HttpModule,
    ChartsModule,
    NgbModule.forRoot(),
    FormsModule,
    AngularMultiSelectModule,
    DatepickerModule.forRoot()
  ],
  providers: [
    HttpRequestsService,
    CheckStatusService,
    DevService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
