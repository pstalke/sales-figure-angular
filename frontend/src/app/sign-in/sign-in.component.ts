import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CheckStatusService } from '../services/check-status.service';

declare var require: any;


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  emailError: boolean;
  passwordError: boolean;

  signIn: (email: string, password: string) => void;

  constructor(private cs: CheckStatusService, private router: Router) {
    this.emailError = false;
    this.passwordError = false;
  }

  ngOnInit() {
    this.signIn = function(email, password) {
      localStorage.setItem('logged', 'true');
      this.cs.checkStatus();
    }
    
        // localStorage.setItem('logged', 'true');
        // localStorage.setItem('userType', 'fan');
        // this.cs.checkStatus();

  }

}
