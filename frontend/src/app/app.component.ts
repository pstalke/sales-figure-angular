import { Component } from '@angular/core';
import { CheckStatusService } from './services/check-status.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public static userLoggedIn = false;
  public static incognito = false;

  getLoggedStatus: () => boolean;
  title = 'app works!';

  constructor(private router: Router, private cs: CheckStatusService) {
    // location.onPopState(() => {
    //     this.router.navigate([`./`], { preserveQueryParams: true });
    // });
    // DevService.log(AppComponent.userLoggedIn);
  }

  ngOnInit() {
    this.cs.checkStatus();
    // this.lsTest = function () {
    //   let test = 'test';
    //   try {
    //     localStorage.setItem(test, test);
    //     localStorage.removeItem(test);
    //     return true;
    //   } catch (e) {
    //     return false;
    //   }
    // };
    // console.log(navigator.userAgent);
    // let mobileFirefox = false;
    // if(navigator.userAgent.indexOf('Mozilla') != -1 && navigator.userAgent.indexOf('Android') != -1) {
    //   mobileFirefox = true;
    // }
    // if (this.lsTest() === true && !mobileFirefox) {
    //   if (location.pathname !== '/forgot_password_sent') {
    //     this.cs.checkStatus();
    //   };
    // } else {
    //   AppComponent.incognito = true;
    //   this.router.navigate([`./nolcl`], { preserveQueryParams: true });
    // }
    this.getLoggedStatus = function () {
      // if (AppComponent.incognito) {
      //   return false;
      // } else {
        return this.cs.getStatus();
      // }
    };

  }
}
