import { Component, OnInit } from '@angular/core';
import { CheckStatusService } from '../services/check-status.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  open: boolean;
  logout: () => void;
  show: boolean;

  constructor(private cs: CheckStatusService, private router: Router) {
    this.open = false;
    this.show = false;
  }

  ngOnInit() {
    this.logout = function() {
      localStorage.setItem('logged', 'false');
      this.cs.checkStatus();
    }
    this.show = true;
  }

}
